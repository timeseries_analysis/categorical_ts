# Categorical Timeseries
Build interpretable NN models in tensorflow2 for predicting and scoring repeatable categorical time series.

- plm-model -> predict an event in the series while knowing the past and future events.

- ar-model -> predict an event in the series while only knowing the past

The architecture and hyperparameters of the networks can be imposed from the command line.
