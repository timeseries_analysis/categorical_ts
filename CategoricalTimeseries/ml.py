import tensorflow as tf
tfk=tf.keras
tfkl=tf.keras.layers

def extendArray(n,arr):
  if len(arr)==1:
    arr=[arr[0] for i in range(n)]
  elif len(arr)!=n:
    raise RuntimeError("Cannot extend array to length {}. Initial length: {}".format(n,len(arr)))
  return arr

def makeCustomModel(outsize, name, args):
    sequential=tfk.models.Sequential(name=name)
    if args.dropout>0.:
      sequential.add( tfkl.Dropout(args.dropout) )
    for n in range(args.num_layers):
      sequential.add( tfkl.Dense(args.num_units[n],kernel_regularizer=args.kernel_regularizer[n],bias_regularizer=args.bias_regularizer[n],use_bias=args.use_bias[n],activation=args.activation[n]) )
    sequential.add( tfkl.Dense(outsize,kernel_regularizer=args.kernel_regularizer[-1],bias_regularizer=args.bias_regularizer[-1],use_bias=args.use_bias[-1],activation='linear') )
    return sequential

class AutoregressiveFit(tf.keras.Model):
	def __init__(self, model, name=''):
		super(AutoregressiveFit, self).__init__(name=name)
		self.model=tfk.models.clone_model(model)
		self.log_softmax = tf.nn.log_softmax
    
	def call(self, input_tensor, training=False):
		x = self.model(input_tensor)
		x = self.log_softmax(x)
		return x

class AutoregressiveEnergy(tf.keras.Model):
  def __init__(self, model, name=''):
    super(AutoregressiveEnergy, self).__init__(name=name)
    self.model=tfk.models.clone_model(model)
    
  def call(self, input_tensor, training=False):
    x = self.model(input_tensor)
    return x  

def makeAutoregressiveFit( cwidth, model, args ):
  if args.num_layers>0:
    args.num_units=extendArray(args.num_layers,args.num_units)
    args.num_activations=extendArray(args.num_layers,args.activation)
    args.kernel_regularizer=extendArray(args.num_layers+1,args.kernel_regularizer)
    args.bias_regularizer=extendArray(args.num_layers+1,args.bias_regularizer)
    args.use_bias=extendArray(args.num_layers+1,args.use_bias)

  skip=args.skip-1
  In0=tfkl.Input(cwidth[-1])    
  l0=In0[:,0:cwidth[skip+1]]

  l0=AutoregressiveFit( model(cwidth[skip+2]-cwidth[skip+1],name="col1_cell",args=args), name="col1")(l0)
  for c in range(skip+2,len(cwidth)-1):
    l=In0[:,0:cwidth[c]]
    l=AutoregressiveFit( model(cwidth[c+1]-cwidth[c],name="col{}_cell".format(c-skip),args=args), name="col{}".format(c-skip))(l)
    l0=tfkl.concatenate([l0,l])
  return tfk.models.Model(In0,l0)

def makePlmFit( cwidth, model, args ):
  if args.num_layers>0:
    args.num_units=extendArray(args.num_layers,args.num_units)
    args.num_activations=extendArray(args.num_layers,args.activation)
    args.kernel_regularizer=extendArray(args.num_layers+1,args.kernel_regularizer)
    args.bias_regularizer=extendArray(args.num_layers+1,args.bias_regularizer)
    args.use_bias=extendArray(args.num_layers+1,args.use_bias)

  skip=args.skip-1
  In0=tfkl.Input(cwidth[-1])    
  l0=In0[:,0:cwidth[skip+1]]

  l0=AutoregressiveFit( model(cwidth[skip+2]-cwidth[skip+1],name="col1_cell",args=args), name="col1")(l0)
  for c in range(skip+2,len(cwidth)-1):
    l=tfkl.concatenate( [ In0[:,0:cwidth[c]],In0[:,cwidth[c+1]:] ])
    l=AutoregressiveFit( model(cwidth[c+1]-cwidth[c],name="col{}_cell".format(c-skip),args=args), name="col{}".format(c-skip))(l)
    l0=tfkl.concatenate([l0,l])
  return tfk.models.Model(In0,l0)

lossFit=lambda y,yt:-tf.reduce_mean(tf.reduce_sum(tf.multiply(y,yt),axis=1))

def run(model,train_data,args):
  from CategoricalTimeseries.parsers import get_optimizer
  from CategoricalTimeseries.callbacks import LRFinder
  from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
  lossFit=lambda y,yt:-tf.reduce_mean(tf.reduce_sum(tf.multiply(y,yt),axis=1))
  optimizer=get_optimizer(args.optimizer,args.learning_rate,args.learning_decay,args.learning_momentum,args.learning_nesterov,args.learning_rho,args.learning_epsilon)
  model.compile(optimizer=optimizer, loss=lossFit)

  if args.optimize_lr:
    checkpoint=ModelCheckpoint("{}/optimized_weights_lrfinder".format(args.outfolder), monitor='loss', verbose=0, save_best_only=True, save_weights_only=True, mode='auto', save_freq='epoch')
    lr_finder = LRFinder(min_lr=1e-6, max_lr=0.3,reload_weights=False)
    model.fit(train_data, callbacks=[lr_finder,checkpoint], epochs=20, shuffle=True, batch_size=args.batch_size)
  
  else:
    callbacks=[]
    callbacks.append( ModelCheckpoint("{}/optimized_weights".format(args.outfolder), monitor='loss', verbose=0, save_best_only=True, save_weights_only=True, mode='auto', save_freq='epoch') )
    if args.patience:
      callbacks.append( EarlyStopping(monitor=args.monitor, patience=args.patience, verbose=0, mode='auto') )
    callbacks.append( ReduceLROnPlateau(factor=0.5,patience=20,monitor='loss') )

    if args.initial_weights is not None:
      try:
        model.load_weights(args.initial_weights)
      except:
        raise RuntimeError("Cannot load initial weights")

    model.fit(train_data, callbacks=callbacks, epochs=args.epochs, batch_size=args.batch_size)
    model.save( '{}/plmModel'.format(args.outfolder) )
