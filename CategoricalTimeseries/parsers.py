
def save_cmd_args(args,filename):
  from tensorflow.keras.regularizers import L1L2
  import json
  class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
      if isinstance(obj, L1L2):
        return obj.get_config()
      return json.JSONEncoder.default(self, obj)

  with open(filename,'w') as jsonargs:
    json.dump(vars(args),fp=jsonargs)
#    jsonargs.write("{}\n".format(json.dumps(vars(args), indent=4, sort_keys=True, cls = CustomEncoder)))

def str2regularizer(v):
  from tensorflow.keras.regularizers import l1,l2,l1_l2
  if v is None:
    return None
  args=v[v.find("(")+1:v.find(")")].split(';')
  if 'l1l2' in v and len(args)==2:
    return l1_l2(float(args[0]),float(args[1]))
  if 'l1' in v:
    return l1(float(args[0]))
  if 'l2' in v:
    return l2(float(args[0]))
  else:
    raise argparse.ArgumentTypeError('Regularizer type must be one of the following: l1, l2 or l1l2')

def str2bool(v):
  if isinstance(v, bool):
    return v
  if v.lower() in ('yes', 'true', 't', 'y', '1'):
    return True
  elif v.lower() in ('no', 'false', 'f', 'n', '0'):
    return False
  else:
    raise argparse.ArgumentTypeError('Boolean value expected.')

def str2prob(v):
  try:
    f=float(v)
  except:
    raise argparse.ArgumentTypeError('Float value expected.')
  if f<0. or f>1.:
    raise argparse.ArgumentTypeError('A value between zero and one was expected')
  return f
 
def get_optimizer(name,lr,ld,lm,ln,lh,le):
  from tensorflow.keras.optimizers import SGD,Adam,Adadelta,RMSprop
  if name=="SGD":
    return SGD(learning_rate=lr, momentum=lm, decay=ld, nesterov=ln)
  if name=="Adam":
    return Adam(learning_rate=lr, epsilon=le, decay=ld)
  if name=="Adadelta":
    return Adadelta(learning_rate=lr, rho=lh, epsilon=le, decay=ld)
  if name=="RMSprop":
    return RMSprop(learning_rate=lr, rho=lh, epsilon=le, decay=ld)

def process_args(args):
  import os
  if args.outfolder is None:
    args.outfolder=".".join( args.x.split(".")[:-1] )
  if not os.path.exists(args.outfolder):
    os.makedirs(args.outfolder)

  save_cmd_args(args,args.outfolder+'/args.json')

  args.kernel_regularizer=[str2regularizer(kr) for kr in args.kernel_regularizer]
  args.bias_regularizer=[str2regularizer(br) for br in args.bias_regularizer]
  args.use_bias=[str2bool(ub) for ub in args.use_bias]
  args.dropout=str2prob(args.dropout)
  
  return args

def seqs_to_sparse(seqs,profile,skip=0):
  import tensorflow as tf
  indices=[]
  for n,seq in enumerate(seqs):
      for col,aa in enumerate(seq[skip:]):
          try:
              indices.append( [n,profile.index[col+skip][aa]+profile.cumulative[col+skip]-profile.cumulative[skip]] )
          except:
              print("I do not know how to encode symbol {} in column {}. This position will remain empty.".format(aa,col+skip),file=sys.stderr)
  values=[1. for idx in indices]
  dense_shape=[ len(seqs),profile.cumulative[-1]-profile.cumulative[skip] ]
  return tf.SparseTensor(indices=indices, values=values, dense_shape=dense_shape)

def parse_data(args):
  from fastaUtils.fasta import parse_fasta 
  from fastaUtils.profiles import profile_data,parse_profile
  import numpy as np
  import tensorflow as tf
  profile=profile_data( parse_profile( args.profile ) )
  cwidth=profile.cumulative

  # x=np.load(args.x).astype(float)
  x=parse_fasta(args.x)
  x=[seq.seq for seq in x]
  y=seqs_to_sparse( x,profile,skip=args.skip )
  x=seqs_to_sparse( x,profile )

  if args.sample_weights is not None:
    sw=np.loadtxt(args.sample_weights,dtype=float)
  else:
    sw=np.ones( shape=(x.shape[0],) )

  sw=tf.convert_to_tensor(sw)
  
  buffer_size=x.shape[0]
  train_data=tf.data.Dataset.from_tensor_slices( (x,y,sw) ).shuffle(buffer_size,reshuffle_each_iteration=True).batch(args.batch_size).map( lambda x,y,w: (tf.sparse.to_dense(x),tf.sparse.to_dense(y),w) ).prefetch(tf.data.AUTOTUNE)
  return train_data,cwidth

def default_parser(prog):
  import argparse
  import time
  
  parser = argparse.ArgumentParser(prog=prog,formatter_class=argparse.ArgumentDefaultsHelpFormatter,add_help=False)
  parser.add_argument('--from-json', type=str,help='Load default parameters from a json file')
  args, remaining = parser.parse_known_args()
  defaults = {}
  if args.from_json:
    import json
    try:
      with open(args.from_json,'r') as infile:
        defaults = json.load(infile)
    except ValueError:
      raise ValueError('Error: Could not parse json file')
  parser.add_argument('-h','--help', action='help',help='Print this help message')
  # backend
  setup_group = parser.add_argument_group('Setup environment')
  setup_group.add_argument('--seed', dest='seed', type=int,default=int(round(time.time())),help='seed')
  setup_group.add_argument('-N','--name', dest='jobname', type=str,default="", help='Task name or identifier')
  setup_group.add_argument('-loglevel', dest='loglevel', type=str, choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"], default="ERROR", help='Logging: log level')
  setup_group.add_argument('-logfile', dest='logfile', type=str, default=None, help='Logging: log file. Default: print log to stdout')
  setup_group.add_argument('-verbose', dest='verbose', action='store_true', default=False, help='Print information during training')  
  # data
  data_group = parser.add_argument_group('Data')
  data_group.add_argument('-p', dest='profile', type=str,required=True, help='Profile')
  data_group.add_argument('-x', dest='x', type=str,required=('x' not in defaults), help='Data')
  data_group.add_argument('-valx', dest='valx', type=str,required=False, default=None, help='Validation data')
  data_group.add_argument('--sample-weights', dest='sample_weights', type=str, help='Reweight each input data')
  data_group.add_argument('--val-sample-weights', dest='val_sample_weights', type=str, help='Reweight each validation data')
  data_group.add_argument('--skip', dest='skip', type=int, default=1, help='Skip n features')
  # network description
  network_group = parser.add_argument_group('Network')
  network_group.add_argument('-l','--num-layers', type=int, default=0,help="Number of hidden layers")
  network_group.add_argument('-u','--num-units', type=int,nargs='+', default=[64],help="Units per layer")
  network_group.add_argument('-a','--activation', type=str,nargs='+', default=["softplus"],help="Activation function per layer")
  network_group.add_argument('-br','--bias-regularizer', type=str,nargs='+', default=[None],help="Bias regularizer per layer")
  network_group.add_argument('-kr','--kernel-regularizer', type=str,nargs='+', default=[None],help="Kernel regularizer per layer")
  network_group.add_argument('-ub','--use-bias', type=str,nargs='+', default=[True],help="Decide whether to use biases or no")
  network_group.add_argument('--dropout', type=str, default="0.",help="Dropout value")
  network_group.add_argument('--initial-weights', type=str, default=None,help="Initial parameters")
  # training description
  training_group = parser.add_argument_group('Training')
  training_group=parser.add_argument_group('Fitting options')
  training_group.add_argument('-pt', '--patience', dest='patience', type=int, default=None, help='EarlyStopping patience')
  training_group.add_argument('-m','--monitor', dest='monitor', type=str, default='loss', choices=['categorical_accuracy','val_categorical_accuracy','loss','val_loss'], help='EarlyStopping and Checkpoint monitor observable')
  training_group.add_argument('-ep', '--epochs', dest='epochs', type=int, default=500, help='Epochs')
  training_group.add_argument('-bs', '--batch-size', dest='batch_size', type=int, default=512, help='Batch size')
  training_group.add_argument('--shuffle', dest='shuffle', action='store_true',default=False, help='Shuffle x and y consistently during training')
  training_group.add_argument('-es','--ensable-snapshots', dest='ensamble_snapshots', type=int,default=None, help='Use ensamble sheduler with a given number of periods')
  # optimizer description
  optimizer_group=parser.add_argument_group('Optimizer')
  optimizer_group.add_argument('--optimizer', dest='optimizer', type=str, choices=['Adam','SGD','Adadelta','RMSprop'], default="SGD", help='Optimizer')
  optimizer_group.add_argument('--learning-rate', dest='learning_rate', type=float, default=0.1, help='Learning rate')
  optimizer_group.add_argument('--learning-decay', dest='learning_decay', type=float, default=0., help='Learning decay')
  optimizer_group.add_argument('--learning-momentum', dest='learning_momentum', type=float, default=0., help='Learning momentum (only for SGD optimizer)')
  optimizer_group.add_argument('--learning-nesterov', dest='learning_nesterov', action='store_true', default=False, help='Use Nesterov algorithm (only for SGD optimizer)')
  optimizer_group.add_argument('--learning-rho', dest='learning_rho', type=float, default=0., help='Learning rho  (only for Adadelta optimizer)')
  optimizer_group.add_argument('--learning-epsilon', dest='learning_epsilon', type=float, default=None, help='Learning epsilon  (only for Adadelta optimizer)')
  # output
  output_group = parser.add_argument_group('Output')
  output_group.add_argument('--optimize-lr', dest='optimize_lr', action='store_true', default=False, help='Fit learning rate')
  output_group.add_argument('--outfolder', dest='outfolder', type=str, default=None, help='Folder where to save generated files')
  # info
  info_group = parser.add_argument_group('About')
  info_group.add_argument('--version', action='version', version='%(prog)s 3.0')
  # parse commandline arguments
  parser.set_defaults(**defaults)
  return parser
  args=parser.parse_args(remaining)
